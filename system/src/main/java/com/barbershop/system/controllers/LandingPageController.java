package com.barbershop.system.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LandingPageController {

    @GetMapping("/")
    public String LandingPage(Model model){
        return "index";
    }

    @GetMapping("/scheduling")
    public String SchedulingPage(Model model) {
        return "schedulingView";
    }

    @GetMapping("/services")
    public String ServicesPage(Model model) {
        return "servicesView";
    }
}
